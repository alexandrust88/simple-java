# Lab5- Assembling pipelines from components (25 mins) 

### In this lab we will cover the following topics:

- include
- extend 
- YAML Anchors
- !reference
- customizing Auto DevOps 

### Assembling pipelines from components - Lesson 

[Watch this video ](https://drive.google.com/file/d/12tKtl7fdRNM6ddlip2gDqT7Y5dyfkaLL/view?usp=sharing)

[Assembling pipelines from components](https://docs.google.com/presentation/d/1xgACMlhy91vjKNBjyq3A6SpE8CFD_wO7Xb29y51t7qM/edit?usp=sharing)

### Exercise 1  - Includes 

1. Create CI configuration file, call it `testing-gitlab-ci.yml`. Add two dummy hello world jobs to it, commit it to `master`.
2. Create (`.gitlab-ci.yml`), include `testing-gitlab-ci.yml`, commit to `master`. Run the pipeline and check that the jobs in the include file executed. 
3. Include an official GitLab CI template in your pipeline. 
4. Now we will create a project that will hold CI templates: create another project in your group, create in the root of the repository a directory called `templates`, and add a new CI configuration file in this directory called `.gitlab-ci-template.yml`, add a simple job to it. In your main CI configuration in the original project, include this template, and commit it to `master`. Check that your included job runs succesfully. 


### Exercise 2 - Extends , hidden jobs, named jobs 

1. On the same branch, create a template job, add "hello-world" script and a rule. Create two jobs that extends the template job you created. Each job has a different script and different rule. Run the pipeline and check the log. 
2. Include Auto DevOps template, extend the `build` job in it by creating a new job with the same name (`build`), add a simple echo script to it. Run the pipeline, check the log
3. Include Auto DevOps template, create a new job that extends the `build` job with the `extends` keyword, add a simple echo script that prints this text `Overwrting build job...`. (You will notice that this job name must be different than `build`). Run the pipeline, check the log, search the text you defined in step 2.


### Exercise 3 - YAML Anchors 

1. Create a dummy hello-world hidden job template with YAML anchor , create two jobs that inherit this job template.
2. Create `YAML anchor` for `scripts`, create `YAML anchor` for `variables`, create a job that uses the `scripts` and `variables` from the YAML anchors you created. 

### Exercise 4 - !reference

1. On the same branch, create CI configuration in the root of the repository, call it `setup.yml`, add to `setup.yml` hidden job that has a script, and two variables `VAR_A`, `VAR_B`. 
2. In your main CI configuration, `.gitlab-ci.yml`, include `setup.yml`
3. Create in it a new job with a simple script echo command: `script: - echo TEST`. Add the script from the job defined in step 1 using `!reference` such that it executes before the echo line.
4. Create a job that prints to the console the value of `VAR_A` and `VAR_B`
5. Define new variable `MY_VAR` that gets the value of `VAR_A` and prints it to the console.



### Exercise 5 - Customizing Auto DevOps 

1. Create CI configuration that includes the official `Auto DevOps template`, Disable `SAST`, `DAST` and `Container Scanning` 

