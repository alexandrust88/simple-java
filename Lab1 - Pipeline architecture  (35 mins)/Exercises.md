## Lab1 - Pipeline architecture (35 minutes)

### In this lab we will cover the following topics:

- Stages, Jobs , Options
- Directed Acyclic Graph(DAG)
- Parent/Child pipelines
- Multi Project Pipelines
- Dynamic Pipelines



### Pipeline architecture - Lesson 

[Watch this video ](https://drive.google.com/file/d/1bOg88nZsNuSiP6ON5wSD-d_L0duUJi4T/view?usp=sharing)

[Pipeline architecture deck ](https://docs.google.com/presentation/d/1_5JYIqDJaWyW2kXMfTu6A7HHSYlwspKZcssUbEguwGg/edit#slide=id.g8ae6d35cde_0_0)

### Exercise 1  - Create a basic CI configuration 

1. Create a new .gitlab-ci.yml in the root of the repositiry - from the project overview page, click the Plus icon and select New file . 
2. Add the stages `build`, `test`, `deploy`. 
3. Create jobs: `build` under `build` stage, two test jobs under `test` stage : `test-linux`, `test-mac`, `deploy` job under `deploy` stage. 
4. Each job must include a script and a stage.
5. Set the deploy job to manual - add `when: manual`
6. In the Commit message, add the following message : "Creating .gitlab-ci.yml" 
7. In the `Target Branch`, leave the default branch as-is `master` 
8. Check the pipeline graph -> CI/ CD menu -> Pipelines -> open the first pipeline from the list. 


### Exercise 2 - Directed Acyclic Graph  

1. On the same branch,(`master`), create another build job in the build stage, `build-linux`. 
2. Change the `test-mac` job in a way that it will start run as soon as `build` job completes, even before `build-linux` completes - add `needs: [build]`
3. To test that DAG works, and make sure `test-mac` will run while `build-linux` is running, add a delay of 5 minutes to `build-linux` -> add `when: delayed`, `start_in: 5 minutes`
4. Commit the change.
5. Check the pipeline graph  -> CI/ CD menu -> Pipelines -> open the first pipeline from the list, and notice that `test-mac` job started while a job in the `build` stage is still running. - now you understand what is DAG. 
6. Optional: DAG visulization (`Needs` tab in your pipline graph) will not work, as it requires at least 3 depended jobs. You can add more depended jobs (jobs with `needs: []` keyword) in order to see the DAG visualization tab
7. Clean-up task: remove the `when: delayed` from `build-linux`, so it will not deley your next pipelines.  


### Exercise 3 - Parent-Child Pipeline 

1. On the same branch, create a sub directory in the root of the repository - name it `serviceA`.
2. Open `serviceA` directory, and create in it a CI configuration file (`.gitlab-ci.yml`), which includes only one simple job with `script: echo "This is a child pipeline configuration..." `.
3. In your main CI configuration, add a new job that triggers the child pipeline you just created using the keywords `trigger`, and `include`. 
4. Add `strategy: depend` to this trigger. 
5. Add a rule that the job will run only if there were changes in `serviceA` directory, using the keywords `rules` and `changes`.
6. Check the pipeline graph, expend it to see the downstream `serviceA` child-pipelne. 
7. Change some text in the main CI configuration, commit the change, and see that now `serviceA` job isn't created in the execution. why? 


### Exercise 4 - Multi-project pipeline 

1. In your personal group in the demo system, create a new project, name it `app-frontend`
2. Create in this project a CI configuration file (`.gitlab-ci.yml`), which includes only one simple job with `script: echo "Hello, this is app frontend..." `
3. From your main CI configuration in the original project, still on the master branch, create a bridge job that will trigger a cross-project pipeline in `app-frontend` , name the job `bridge`, use keywords: `trigger`. `project`, `branch`, `strategy`. The `project` value should include the `full path` of the downstream project. 


### Exercise 5 - Dynamic child pipeline 

1. In the root of the repository create a new file, name it `replace.gitlab-ci.yml`, add to this file a simple job with this script: `script: echo "REPLACEME" `
2. In your main CI configuration, still on the master branch, create a job named `setup`. The `setup` job will dynamically create a CI configuration file, and will save it as an artifact.  In a later stage of the pipeline we will run this dynamic job. 
3. The `setup` job should include this script ` - sed "s/REPLACEME/DynamicPipeline/g" replace.gitlab-ci.yml > generated-config.yml ` . This script will copy the text from the file you created in step 1, and will replace the text `REPLACEME` with `DynamicPipeline` , and paste the updated text to a new config file `generated-config.yml` 
4. Save the new file `generated-config.yml` as an artifact 
5. Create a trigger job that will use the config you saved as an artifact to trigger a dynamic pipeline, use the following keywords: `trigger`, `include`, `artifact` and `job` 
6. Commit the change, open the pipeline graph, and check that the job you defined in step one, is now running as a downstream job, and that this job prints to the console `DynamicPipeline`.


### Cleanup 

1. Rename the CI configuration `.gitlab-ci.yml` to `.lab1-gitlab-ci.yml` , you can use this file for reference for the next labs, also use it in future whenever you need CI code examples.
2. Commit this change to `master` branch
