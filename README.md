## CI Labs

### Pipeline architecture  

- Stages, Jobs , Options 
- Directed Acyclic Graph(DAG) 
- Parent/Child pipelines
- Multi Project Pipelines 
- Dynamic Pipelines 
- CI component in Auto Devops:



### Using the pipeline Editor and Lint 

- Pipeline Editor
- CI Lint 

### Controlling how and when your jobs run

- Variables
- Predefined Variables 
- Rules 

### Pipelines and Merge Requests 
 
- The Branch Pipeline 
- The Merge Request Pipeline 
- Pipelines for Merge Results 
- Merge Trains 
- Types of CI/CD pipeline runs 

### Assembling pipelines from components

- include
- extend
- YAML Anchors
- !reference
- customizing Auto DevOps

### Holding, securing, and sharing results

The following is a non-exhaustive list of topics to be covered in this lab:

- Job Artifacts
- Container and Package Registries 

### Tips & Tricks for the power user

- Skip CI
- CI Linting
- Avoid Double Pipelines
- Parallel and Matrix Testing 
- Leverage Runner Caching 

