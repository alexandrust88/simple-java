1. [.gitlab-ci.yml - job creates artifact](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100116)
2. [.gitlab-cil.yml - job uses artifact](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100118)
3. Artifacts are used to share files between jobs in different stages (jobs can access artifacts from previous stages). Also, Artifacts are the job output that can be downloaded from the UI. 
