## Lab2 - Pipeline Editor (15 mins)

The pipeline editor is the primary place to edit the GitLab CI/CD configuration in the .gitlab-ci.yml file in the root of your repository. To access the editor, go to CI/CD > Editor.

From the pipeline editor page you can:

- Validate your configuration syntax while editing the file.
- Do a deeper lint of your configuration, that verifies it with any configuration added with the include keyword.
- See a visualization of the current configuration.
- View an expanded version of your configuration.
- Commit the changes to a specific branch.


In this lab we will experiance the Pipeline Editor


### Exercise 1  - Getting started with the Pipeline editor 

1. Make sure you followed the cleanup step in `lab1`.
2. Open the pipeline editor (CICD->Editor), and click the `Create new CI/CD pipepile` button. You will notice that a new `.gitlab-ci.yml` created with initial template. 
3. Check the Visualize tab, and the Lint tab.
4. In Edit tab, remove the stages definition entirly, and add `include:` 
                                    `- template: Auto-DevOps.gitlab-ci.yml`.
6. Check the `View merged YAML` tab. 
7. In the merged YAML, serach for `echo "Compiling the code..."`, to verify your jobs have been merged succcesfully with the template jobs.  

### Cleanup 

1. In Edit tab, scroll down and click cancel. 



